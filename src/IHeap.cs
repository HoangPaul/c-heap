﻿namespace HoangPaul.Heap
{
    interface IHeap<T>
    {
        int Count { get;  }

        void Insert(T x);

        bool Remove(T item);

        T Extract();

        bool Contains(T item);

        T Peek();
    }
}
