﻿using System.Collections;
using System.Collections.Generic;

namespace HoangPaul.Heap
{
    public class Heap<T> : IHeap<T>
    {
        protected List<T> heap;
        protected IComparer comparer;

        public int Count { get { return heap.Count; } }

        public Heap(IComparer comparer)
        {
            this.heap = new List<T>();
            this.comparer = comparer;
        }

        public virtual void Insert(T x)
        {
            int newPos = heap.Count;
            int parentPos = GetParent(newPos);

            heap.Add(x);

            BubbleUp(newPos, parentPos);
        }

        public virtual bool Remove(T item)
        {
            if (!heap.Contains(item))
            {
                return false;
            }
            int rootIndex = heap.IndexOf(item);
            int lastItemIndex = heap.Count - 1;

            SwapValues(rootIndex, lastItemIndex);
            RemoveValue(heap.IndexOf(item));

            BubbleDown(rootIndex);

            return true;
        }

        public virtual T Extract()
        {
            if (heap.Count < 1)
            {
                throw new System.InvalidOperationException();
            }

            T item = heap[0];

            int rootIndex = 0;
            int lastItemIndex = heap.Count - 1;

            SwapValues(rootIndex, lastItemIndex);
            RemoveValue(lastItemIndex);

            BubbleDown(rootIndex);

            return item;
        }

        public virtual bool Contains(T item)
        {
            return heap.Contains(item);
        }

        public virtual T Peek()
        {
            if (heap.Count < 1)
            {
                throw new System.InvalidOperationException();
            }
            return heap[0];
        }

        protected int GetParent(int position)
        {
            int parentPos = -1;
            if (heap.Count != 0 && position != 0)
            {
                parentPos = (position - 1) / 2;
            }
            return parentPos;
        }

        protected Children GetChildren(int position)
        {
            int leftChildIndex = -1, rightChildIndex = -1;
            position++;
            if (position * 2 <= heap.Count)
            {
                leftChildIndex = position * 2 - 1;
            }
            if (position * 2 + 1 <= heap.Count)
            {
                rightChildIndex = position * 2;
            }
            return new Children(leftChildIndex, rightChildIndex);
        }

        protected bool HasChildren(int position)
        {
            return (position + 1) * 2 <= heap.Count;
        }

        protected void BubbleUp(int index, int parentIndex)
        {
            if (parentIndex < 0)
            {
                return;
            }
            while (!IsInOrder(parentIndex, index))
            {
                SwapValues(index, parentIndex);
                index = parentIndex;
                parentIndex = GetParent(index);
                if (parentIndex < 0)
                {
                    break;
                }
            }
        }

        protected void BubbleDown(int rootIndex)
        {
            if (HasChildren(rootIndex))
            {
                Children leavesIndex = GetChildren(rootIndex);
                while (!IsInOrder(rootIndex, leavesIndex))
                {
                    int leafIndex = GetOrderedChild(leavesIndex);
                    SwapValues(leafIndex, rootIndex);
                    rootIndex = leafIndex;
                    leavesIndex = GetChildren(rootIndex);
                    if (leavesIndex.leftChild < 0)
                    {
                        break;
                    }
                }
            }
        }

        protected int GetOrderedChild(Children children)
        {
            if (children.rightChild < 0)
            {
                return children.leftChild;
            }
            if (IsInOrder(children.leftChild, children.rightChild))
            {
                return children.leftChild;
            }
            else
            {
                return children.rightChild;
            }
        }

        protected bool IsInOrder(int parentPos, Children children)
        {
            if (children.rightChild < 0)
            {
                return IsInOrder(parentPos, children.leftChild);
            }
            return IsInOrder(parentPos, children.leftChild) && IsInOrder(parentPos, children.rightChild);
        }

        protected bool IsInOrder(int parentPos, int childPos)
        {
            return comparer.Compare(heap[parentPos], heap[childPos]) < 0;
        }

        protected virtual void SwapValues(int posX, int posY)
        {
            T temp = heap[posX];
            heap[posX] = heap[posY];
            heap[posY] = temp;
        }

        protected virtual void RemoveValue(int index)
        {
            heap.RemoveAt(index);
        }

        protected struct Children
        {
            public readonly int leftChild;
            public readonly int rightChild;
            public Children(int _leftChild, int _rightChild)
            {
                leftChild = _leftChild;
                rightChild = _rightChild;
            }
        }
    }
}
