using System.Collections;
using System.Collections.Generic;

namespace HoangPaul.Heap
{
    public class DictionaryHeap<T> : Heap<T>
    {
        private Dictionary<T, int> dictionary;

        public DictionaryHeap(IComparer comparer) : base(comparer)
        {
            this.dictionary = new Dictionary<T, int>();
        }

        public override void Insert(T x)
        {
            int newPos = heap.Count;

            dictionary.Add(x, newPos);

            base.Insert(x);
            
        }

        public override bool Remove(T item)
        {
            if (!dictionary.ContainsKey(item))
            {
                return false;
            }
            int rootIndex = dictionary[item];
            int lastItemIndex = heap.Count - 1;

            SwapValues(rootIndex, lastItemIndex);
            RemoveValue(dictionary[item]);

            BubbleDown(rootIndex);

            return true;
        }

        public override T Extract()
        {
            if (heap.Count < 1)
            {
                throw new System.InvalidOperationException();
            }

            T item = heap[0];

            Remove(item);

            return item;
        }

        public override bool Contains(T item)
        {
            return dictionary.ContainsKey(item);
        }

        protected override void SwapValues(int posX, int posY)
        {
            T temp = heap[posX];
            heap[posX] = heap[posY];
            heap[posY] = temp;
            dictionary[heap[posX]] = posX;
            dictionary[heap[posY]] = posY;
        }

        protected override void RemoveValue(int index)
        {
            dictionary.Remove(heap[index]);
            heap.RemoveAt(index);
        }
    }
}